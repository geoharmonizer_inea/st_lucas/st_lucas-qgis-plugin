#!/bin/bash -e

VENV=/tmp/st_lucas_download_manager
python3 -m venv $VENV
source $VENV/bin/activate

pip3 install -r help/requirements.txt
pip3 install st_lucas
# pip3 install git+https://gitlab.com/geoharmonizer_inea/st_lucas/st_lucas-python-package.git@main

pv=$(python3 -V | cut -d' ' -f 2 | cut -d'.' -f 1,2)
LIB=$VENV/lib/python$pv/site-packages

cp -r $LIB/st_lucas .
find st_lucas -name __pycache__ | xargs rm -rf

pb_tool zip

rm -rf st_lucas
deactivate
rm -rf $VENV

exit 0
