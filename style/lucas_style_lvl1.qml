<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis symbologyReferenceScale="-1" minScale="100000000" styleCategories="AllStyleCategories" version="3.22.4-Białowieża" readOnly="0" simplifyDrawingTol="1" simplifyAlgorithm="0" maxScale="0" simplifyLocal="1" simplifyMaxScale="1" hasScaleBasedVisibilityFlag="0" simplifyDrawingHints="0" labelsEnabled="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal limitMode="0" endField="" startField="" accumulate="0" mode="0" enabled="0" fixedDuration="0" durationUnit="min" durationField="" startExpression="" endExpression="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 symbollevels="0" forceraster="0" enableorderby="0" type="RuleRenderer" referencescale="-1">
    <rules key="{0231b674-7213-4204-9eda-63fd17707328}">
      <rule symbol="0" key="{4f8609a1-4425-48af-84ae-48383d646e47}" filter="&quot;lc1_h&quot; ILIKE 'A%' OR &quot;lc1_h_l3_missing&quot; ILIKE 'A%'" label="Artificial Land"/>
      <rule symbol="1" key="{a154ca4a-dabe-472c-9c25-ed0a2a06a468}" filter="&quot;lc1_h&quot; ILIKE 'B%' OR &quot;lc1_h_l3_missing&quot; ILIKE 'B%'" label="Cropland"/>
      <rule symbol="2" key="{24c82d90-404c-49d0-bfde-65ce1a5963da}" filter="&quot;lc1_h&quot; ILIKE 'C%' OR &quot;lc1_h_l3_missing&quot; ILIKE 'C%'" label="Woodland"/>
      <rule symbol="3" key="{2570bc7a-7e19-4405-80af-a79118310b7a}" filter="&quot;lc1_h&quot; ILIKE 'D%' OR &quot;lc1_h_l3_missing&quot; ILIKE 'D%'" label="Shrubland"/>
      <rule symbol="4" key="{2d82e20a-c4d4-4779-a5fa-01aaaed38293}" filter="&quot;lc1_h&quot; ILIKE 'E%' OR &quot;lc1_h_l3_missing&quot; ILIKE 'E%'" label="Grassland"/>
      <rule symbol="5" key="{b4ffebe7-648d-4784-b83a-8702b068911a}" filter="&quot;lc1_h&quot; ILIKE 'F%' OR &quot;lc1_h_l3_missing&quot; ILIKE 'F%'" label="Bare Land and Lichen/Moss"/>
      <rule symbol="6" key="{8886eae2-4c6c-4aec-9a66-462865f09adb}" filter="&quot;lc1_h&quot; ILIKE 'G%' OR &quot;lc1_h_l3_missing&quot; ILIKE 'G%'" label="Water Areas"/>
      <rule symbol="7" key="{f99a9926-0016-49fe-a545-90e40871c8d7}" filter="&quot;lc1_h&quot; ILIKE 'H%' OR &quot;lc1_h_l3_missing&quot; ILIKE 'H%'" label="Wetlands"/>
      <rule symbol="8" key="{6dfd421f-5a83-4688-a88c-7b3920ea3408}" filter="&quot;lc1_h&quot; NOT ILIKE 'A%' AND&#xd;&#xa;&quot;lc1_h&quot; NOT ILIKE 'B%' AND&#xd;&#xa;&quot;lc1_h&quot; NOT ILIKE 'C%' AND&#xd;&#xa;&quot;lc1_h&quot; NOT ILIKE 'D%' AND&#xd;&#xa;&quot;lc1_h&quot; NOT ILIKE 'E%' AND&#xd;&#xa;&quot;lc1_h&quot; NOT ILIKE 'F%' AND&#xd;&#xa;&quot;lc1_h&quot; NOT ILIKE 'G%' AND&#xd;&#xa;&quot;lc1_h&quot; NOT ILIKE 'H%' AND&#xd;&#xa;&quot;lc1_h_l3_missing&quot; NOT ILIKE 'A%' AND&#xd;&#xa;&quot;lc1_h_l3_missing&quot; NOT ILIKE 'B%' AND &#xd;&#xa;&quot;lc1_h_l3_missing&quot; NOT ILIKE 'C%' AND &#xd;&#xa;&quot;lc1_h_l3_missing&quot; NOT ILIKE 'D%' AND &#xd;&#xa;&quot;lc1_h_l3_missing&quot; NOT ILIKE 'E%' AND&#xd;&#xa;&quot;lc1_h_l3_missing&quot; NOT ILIKE 'F%' AND &#xd;&#xa;&quot;lc1_h_l3_missing&quot; NOT ILIKE 'G%' AND &#xd;&#xa;&quot;lc1_h_l3_missing&quot; NOT ILIKE 'H%'" label="Not classified"/>
    </rules>
    <symbols>
      <symbol force_rhr="0" clip_to_extent="1" type="marker" alpha="1" name="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" enabled="1" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="255,0,0,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="255,0,0,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="name">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="expression" value="if (&quot;photo_point&quot; = 1, 'circle', 'square')"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="marker" alpha="1" name="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" enabled="1" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="153,230,0,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="153,230,0,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="name">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="expression" value="if (&quot;photo_point&quot; = 1, 'circle', 'square')"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="marker" alpha="1" name="2">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" enabled="1" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="55,168,0,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="55,168,0,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="name">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="expression" value="if (&quot;photo_point&quot; = 1, 'circle', 'square')"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="marker" alpha="1" name="3">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" enabled="1" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="36,116,0,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="36,116,0,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="name">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="expression" value="if (&quot;photo_point&quot; = 1, 'circle', 'square')"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="marker" alpha="1" name="4">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" enabled="1" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="230,230,0,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="230,230,0,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="name">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="expression" value="if (&quot;photo_point&quot; = 1, 'circle', 'square')"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="marker" alpha="1" name="5">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" enabled="1" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="116,77,0,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="116,77,0,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="name">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="expression" value="if (&quot;photo_point&quot; = 1, 'circle', 'square')"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="marker" alpha="1" name="6">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" enabled="1" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="0,197,255,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="0,197,255,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="name">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="expression" value="if (&quot;photo_point&quot; = 1, 'circle', 'square')"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="marker" alpha="1" name="7">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" enabled="1" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="0,169,230,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="0,169,230,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="name">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="expression" value="if (&quot;photo_point&quot; = 1, 'circle', 'square')"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" clip_to_extent="1" type="marker" alpha="1" name="8">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" name="name" value=""/>
            <Option name="properties"/>
            <Option type="QString" name="type" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" enabled="1" class="SimpleMarker" locked="0">
          <Option type="Map">
            <Option type="QString" name="angle" value="0"/>
            <Option type="QString" name="cap_style" value="square"/>
            <Option type="QString" name="color" value="0,0,0,255"/>
            <Option type="QString" name="horizontal_anchor_point" value="1"/>
            <Option type="QString" name="joinstyle" value="bevel"/>
            <Option type="QString" name="name" value="circle"/>
            <Option type="QString" name="offset" value="0,0"/>
            <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="offset_unit" value="MM"/>
            <Option type="QString" name="outline_color" value="35,35,35,255"/>
            <Option type="QString" name="outline_style" value="solid"/>
            <Option type="QString" name="outline_width" value="0"/>
            <Option type="QString" name="outline_width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="outline_width_unit" value="MM"/>
            <Option type="QString" name="scale_method" value="diameter"/>
            <Option type="QString" name="size" value="2"/>
            <Option type="QString" name="size_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            <Option type="QString" name="size_unit" value="MM"/>
            <Option type="QString" name="vertical_anchor_point" value="1"/>
          </Option>
          <prop v="0" k="angle"/>
          <prop v="square" k="cap_style"/>
          <prop v="0,0,0,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option type="Map" name="properties">
                <Option type="Map" name="name">
                  <Option type="bool" name="active" value="true"/>
                  <Option type="QString" name="expression" value="if (&quot;photo_point&quot; = 1, 'circle', 'square')"/>
                  <Option type="int" name="type" value="3"/>
                </Option>
              </Option>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option type="List" name="dualview/previewExpressions">
        <Option type="QString" value="&quot;nuts0&quot;"/>
      </Option>
      <Option type="QString" name="embeddedWidgets/count" value="0"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory penColor="#000000" backgroundColor="#ffffff" sizeType="MM" lineSizeScale="3x:0,0,0,0,0,0" showAxis="1" enabled="0" sizeScale="3x:0,0,0,0,0,0" diagramOrientation="Up" direction="0" spacingUnit="MM" maxScaleDenominator="1e+08" width="15" rotationOffset="270" height="15" backgroundAlpha="255" barWidth="5" opacity="1" spacing="5" labelPlacementMethod="XHeight" minimumSize="0" scaleBasedVisibility="0" minScaleDenominator="0" lineSizeType="MM" scaleDependency="Area" penAlpha="255" penWidth="0" spacingUnitScale="3x:0,0,0,0,0,0">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
      <attribute field="" color="#000000" label=""/>
      <axisSymbol>
        <symbol force_rhr="0" clip_to_extent="1" type="line" alpha="1" name="">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer pass="0" enabled="1" class="SimpleLine" locked="0">
            <Option type="Map">
              <Option type="QString" name="align_dash_pattern" value="0"/>
              <Option type="QString" name="capstyle" value="square"/>
              <Option type="QString" name="customdash" value="5;2"/>
              <Option type="QString" name="customdash_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="customdash_unit" value="MM"/>
              <Option type="QString" name="dash_pattern_offset" value="0"/>
              <Option type="QString" name="dash_pattern_offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="dash_pattern_offset_unit" value="MM"/>
              <Option type="QString" name="draw_inside_polygon" value="0"/>
              <Option type="QString" name="joinstyle" value="bevel"/>
              <Option type="QString" name="line_color" value="35,35,35,255"/>
              <Option type="QString" name="line_style" value="solid"/>
              <Option type="QString" name="line_width" value="0.26"/>
              <Option type="QString" name="line_width_unit" value="MM"/>
              <Option type="QString" name="offset" value="0"/>
              <Option type="QString" name="offset_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="offset_unit" value="MM"/>
              <Option type="QString" name="ring_filter" value="0"/>
              <Option type="QString" name="trim_distance_end" value="0"/>
              <Option type="QString" name="trim_distance_end_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_end_unit" value="MM"/>
              <Option type="QString" name="trim_distance_start" value="0"/>
              <Option type="QString" name="trim_distance_start_map_unit_scale" value="3x:0,0,0,0,0,0"/>
              <Option type="QString" name="trim_distance_start_unit" value="MM"/>
              <Option type="QString" name="tweak_dash_pattern_on_corners" value="0"/>
              <Option type="QString" name="use_custom_dash" value="0"/>
              <Option type="QString" name="width_map_unit_scale" value="3x:0,0,0,0,0,0"/>
            </Option>
            <prop v="0" k="align_dash_pattern"/>
            <prop v="square" k="capstyle"/>
            <prop v="5;2" k="customdash"/>
            <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
            <prop v="MM" k="customdash_unit"/>
            <prop v="0" k="dash_pattern_offset"/>
            <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
            <prop v="MM" k="dash_pattern_offset_unit"/>
            <prop v="0" k="draw_inside_polygon"/>
            <prop v="bevel" k="joinstyle"/>
            <prop v="35,35,35,255" k="line_color"/>
            <prop v="solid" k="line_style"/>
            <prop v="0.26" k="line_width"/>
            <prop v="MM" k="line_width_unit"/>
            <prop v="0" k="offset"/>
            <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
            <prop v="MM" k="offset_unit"/>
            <prop v="0" k="ring_filter"/>
            <prop v="0" k="trim_distance_end"/>
            <prop v="3x:0,0,0,0,0,0" k="trim_distance_end_map_unit_scale"/>
            <prop v="MM" k="trim_distance_end_unit"/>
            <prop v="0" k="trim_distance_start"/>
            <prop v="3x:0,0,0,0,0,0" k="trim_distance_start_map_unit_scale"/>
            <prop v="MM" k="trim_distance_start_unit"/>
            <prop v="0" k="tweak_dash_pattern_on_corners"/>
            <prop v="0" k="use_custom_dash"/>
            <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" name="name" value=""/>
                <Option name="properties"/>
                <Option type="QString" name="type" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" dist="0" showAll="1" priority="0" zIndex="0" linePlacementFlags="18" placement="0">
    <properties>
      <Option type="Map">
        <Option type="QString" name="name" value=""/>
        <Option name="properties"/>
        <Option type="QString" name="type" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field name="fid" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="point_id" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="nuts0" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="nuts1" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="nuts2" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="nuts3" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="survey_date" configurationFlags="None">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="car_latitude" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="car_longitude" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="car_ew" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="gps_proj" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="gps_prec" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="gps_lat" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="gps_long" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="gps_altitude" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="th_lat" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="th_long" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="dist_thr_grid" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="obs_dist" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="obs_direct" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="obs_type" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="obs_radius" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lc1" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lc1_h" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lc1_h_l3_missing" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lc1_h_l3_missing_level" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lc1_spec" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lc1_perc" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lc1_perc_cls" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lc2" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lc2_h" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lc2_h_l3_missing" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lc2_h_l3_missing_level" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lc2_spec" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lc2_perc" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lc2_perc_cls" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lu1" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lu1_h" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lu1_type" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lu1_perc" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lu1_perc_cls" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lu2" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lu2_h" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lu2_type" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lu2_perc" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lu2_perc_cls" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="parcel_area_ha" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="tree_height_survey" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="tree_height_maturity" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="feature_width" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lm_plough_slope" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lm_plough_direct" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lm_stone_walls" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lm_grass_margins" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cprn_cando" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cprn_lc" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cprn_lc1n" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cprnc_lc1e" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cprnc_lc1s" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cprnc_lc1w" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cprn_lc1n_brdth" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cprn_lc1e_brdth" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cprn_lc1s_brdth" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cprn_lc1w_brdth" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cprn_lc1n_next" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cprn_lc1e_next" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cprn_lc1s_next" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cprn_lc1w_next" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cprn_urban" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cprn_impervious_perc" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="inspire_plcc1" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="inspire_plcc2" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="inspire_plcc3" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="inspire_plcc4" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="inspire_plcc5" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="inspire_plcc6" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="inspire_plcc7" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="inspire_plcc8" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="eunis_complex" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="grassland_sample" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="grass_cando" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="grazing" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="wm" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="wm_source" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="wm_type" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="wm_delivery" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="soil_taken" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="erosion_cando" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="bio_sample" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="soil_bio_taken" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="bulk0_10_sample" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="soil_blk_0_10_taken" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="bulk10_20_sample" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="soil_blk_10_20_taken" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="bulk20_30_sample" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="soil_blk_20_30_taken" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="standard_sample" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="soil_std_taken" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="organic_sample" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="soil_org_depth_cando" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="office_pi" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="pi_extension" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lndmng_plough" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="special_status" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lc_lu_special_remark" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="soil_stones_perc" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="soil_stones_perc_cls" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="photo_point" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="photo_north" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="photo_east" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="photo_south" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="photo_west" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="crop_residues" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="transect" configurationFlags="None">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ex_ante" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="survey_year" configurationFlags="None">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" field="fid" name=""/>
    <alias index="1" field="point_id" name=""/>
    <alias index="2" field="nuts0" name=""/>
    <alias index="3" field="nuts1" name=""/>
    <alias index="4" field="nuts2" name=""/>
    <alias index="5" field="nuts3" name=""/>
    <alias index="6" field="survey_date" name=""/>
    <alias index="7" field="car_latitude" name=""/>
    <alias index="8" field="car_longitude" name=""/>
    <alias index="9" field="car_ew" name=""/>
    <alias index="10" field="gps_proj" name=""/>
    <alias index="11" field="gps_prec" name=""/>
    <alias index="12" field="gps_lat" name=""/>
    <alias index="13" field="gps_long" name=""/>
    <alias index="14" field="gps_altitude" name=""/>
    <alias index="15" field="th_lat" name=""/>
    <alias index="16" field="th_long" name=""/>
    <alias index="17" field="dist_thr_grid" name=""/>
    <alias index="18" field="obs_dist" name=""/>
    <alias index="19" field="obs_direct" name=""/>
    <alias index="20" field="obs_type" name=""/>
    <alias index="21" field="obs_radius" name=""/>
    <alias index="22" field="lc1" name=""/>
    <alias index="23" field="lc1_h" name=""/>
    <alias index="24" field="lc1_h_l3_missing" name=""/>
    <alias index="25" field="lc1_h_l3_missing_level" name=""/>
    <alias index="26" field="lc1_spec" name=""/>
    <alias index="27" field="lc1_perc" name=""/>
    <alias index="28" field="lc1_perc_cls" name=""/>
    <alias index="29" field="lc2" name=""/>
    <alias index="30" field="lc2_h" name=""/>
    <alias index="31" field="lc2_h_l3_missing" name=""/>
    <alias index="32" field="lc2_h_l3_missing_level" name=""/>
    <alias index="33" field="lc2_spec" name=""/>
    <alias index="34" field="lc2_perc" name=""/>
    <alias index="35" field="lc2_perc_cls" name=""/>
    <alias index="36" field="lu1" name=""/>
    <alias index="37" field="lu1_h" name=""/>
    <alias index="38" field="lu1_type" name=""/>
    <alias index="39" field="lu1_perc" name=""/>
    <alias index="40" field="lu1_perc_cls" name=""/>
    <alias index="41" field="lu2" name=""/>
    <alias index="42" field="lu2_h" name=""/>
    <alias index="43" field="lu2_type" name=""/>
    <alias index="44" field="lu2_perc" name=""/>
    <alias index="45" field="lu2_perc_cls" name=""/>
    <alias index="46" field="parcel_area_ha" name=""/>
    <alias index="47" field="tree_height_survey" name=""/>
    <alias index="48" field="tree_height_maturity" name=""/>
    <alias index="49" field="feature_width" name=""/>
    <alias index="50" field="lm_plough_slope" name=""/>
    <alias index="51" field="lm_plough_direct" name=""/>
    <alias index="52" field="lm_stone_walls" name=""/>
    <alias index="53" field="lm_grass_margins" name=""/>
    <alias index="54" field="cprn_cando" name=""/>
    <alias index="55" field="cprn_lc" name=""/>
    <alias index="56" field="cprn_lc1n" name=""/>
    <alias index="57" field="cprnc_lc1e" name=""/>
    <alias index="58" field="cprnc_lc1s" name=""/>
    <alias index="59" field="cprnc_lc1w" name=""/>
    <alias index="60" field="cprn_lc1n_brdth" name=""/>
    <alias index="61" field="cprn_lc1e_brdth" name=""/>
    <alias index="62" field="cprn_lc1s_brdth" name=""/>
    <alias index="63" field="cprn_lc1w_brdth" name=""/>
    <alias index="64" field="cprn_lc1n_next" name=""/>
    <alias index="65" field="cprn_lc1e_next" name=""/>
    <alias index="66" field="cprn_lc1s_next" name=""/>
    <alias index="67" field="cprn_lc1w_next" name=""/>
    <alias index="68" field="cprn_urban" name=""/>
    <alias index="69" field="cprn_impervious_perc" name=""/>
    <alias index="70" field="inspire_plcc1" name=""/>
    <alias index="71" field="inspire_plcc2" name=""/>
    <alias index="72" field="inspire_plcc3" name=""/>
    <alias index="73" field="inspire_plcc4" name=""/>
    <alias index="74" field="inspire_plcc5" name=""/>
    <alias index="75" field="inspire_plcc6" name=""/>
    <alias index="76" field="inspire_plcc7" name=""/>
    <alias index="77" field="inspire_plcc8" name=""/>
    <alias index="78" field="eunis_complex" name=""/>
    <alias index="79" field="grassland_sample" name=""/>
    <alias index="80" field="grass_cando" name=""/>
    <alias index="81" field="grazing" name=""/>
    <alias index="82" field="wm" name=""/>
    <alias index="83" field="wm_source" name=""/>
    <alias index="84" field="wm_type" name=""/>
    <alias index="85" field="wm_delivery" name=""/>
    <alias index="86" field="soil_taken" name=""/>
    <alias index="87" field="erosion_cando" name=""/>
    <alias index="88" field="bio_sample" name=""/>
    <alias index="89" field="soil_bio_taken" name=""/>
    <alias index="90" field="bulk0_10_sample" name=""/>
    <alias index="91" field="soil_blk_0_10_taken" name=""/>
    <alias index="92" field="bulk10_20_sample" name=""/>
    <alias index="93" field="soil_blk_10_20_taken" name=""/>
    <alias index="94" field="bulk20_30_sample" name=""/>
    <alias index="95" field="soil_blk_20_30_taken" name=""/>
    <alias index="96" field="standard_sample" name=""/>
    <alias index="97" field="soil_std_taken" name=""/>
    <alias index="98" field="organic_sample" name=""/>
    <alias index="99" field="soil_org_depth_cando" name=""/>
    <alias index="100" field="office_pi" name=""/>
    <alias index="101" field="pi_extension" name=""/>
    <alias index="102" field="lndmng_plough" name=""/>
    <alias index="103" field="special_status" name=""/>
    <alias index="104" field="lc_lu_special_remark" name=""/>
    <alias index="105" field="soil_stones_perc" name=""/>
    <alias index="106" field="soil_stones_perc_cls" name=""/>
    <alias index="107" field="photo_point" name=""/>
    <alias index="108" field="photo_north" name=""/>
    <alias index="109" field="photo_east" name=""/>
    <alias index="110" field="photo_south" name=""/>
    <alias index="111" field="photo_west" name=""/>
    <alias index="112" field="crop_residues" name=""/>
    <alias index="113" field="transect" name=""/>
    <alias index="114" field="ex_ante" name=""/>
    <alias index="115" field="survey_year" name=""/>
  </aliases>
  <defaults>
    <default applyOnUpdate="0" field="fid" expression=""/>
    <default applyOnUpdate="0" field="point_id" expression=""/>
    <default applyOnUpdate="0" field="nuts0" expression=""/>
    <default applyOnUpdate="0" field="nuts1" expression=""/>
    <default applyOnUpdate="0" field="nuts2" expression=""/>
    <default applyOnUpdate="0" field="nuts3" expression=""/>
    <default applyOnUpdate="0" field="survey_date" expression=""/>
    <default applyOnUpdate="0" field="car_latitude" expression=""/>
    <default applyOnUpdate="0" field="car_longitude" expression=""/>
    <default applyOnUpdate="0" field="car_ew" expression=""/>
    <default applyOnUpdate="0" field="gps_proj" expression=""/>
    <default applyOnUpdate="0" field="gps_prec" expression=""/>
    <default applyOnUpdate="0" field="gps_lat" expression=""/>
    <default applyOnUpdate="0" field="gps_long" expression=""/>
    <default applyOnUpdate="0" field="gps_altitude" expression=""/>
    <default applyOnUpdate="0" field="th_lat" expression=""/>
    <default applyOnUpdate="0" field="th_long" expression=""/>
    <default applyOnUpdate="0" field="dist_thr_grid" expression=""/>
    <default applyOnUpdate="0" field="obs_dist" expression=""/>
    <default applyOnUpdate="0" field="obs_direct" expression=""/>
    <default applyOnUpdate="0" field="obs_type" expression=""/>
    <default applyOnUpdate="0" field="obs_radius" expression=""/>
    <default applyOnUpdate="0" field="lc1" expression=""/>
    <default applyOnUpdate="0" field="lc1_h" expression=""/>
    <default applyOnUpdate="0" field="lc1_h_l3_missing" expression=""/>
    <default applyOnUpdate="0" field="lc1_h_l3_missing_level" expression=""/>
    <default applyOnUpdate="0" field="lc1_spec" expression=""/>
    <default applyOnUpdate="0" field="lc1_perc" expression=""/>
    <default applyOnUpdate="0" field="lc1_perc_cls" expression=""/>
    <default applyOnUpdate="0" field="lc2" expression=""/>
    <default applyOnUpdate="0" field="lc2_h" expression=""/>
    <default applyOnUpdate="0" field="lc2_h_l3_missing" expression=""/>
    <default applyOnUpdate="0" field="lc2_h_l3_missing_level" expression=""/>
    <default applyOnUpdate="0" field="lc2_spec" expression=""/>
    <default applyOnUpdate="0" field="lc2_perc" expression=""/>
    <default applyOnUpdate="0" field="lc2_perc_cls" expression=""/>
    <default applyOnUpdate="0" field="lu1" expression=""/>
    <default applyOnUpdate="0" field="lu1_h" expression=""/>
    <default applyOnUpdate="0" field="lu1_type" expression=""/>
    <default applyOnUpdate="0" field="lu1_perc" expression=""/>
    <default applyOnUpdate="0" field="lu1_perc_cls" expression=""/>
    <default applyOnUpdate="0" field="lu2" expression=""/>
    <default applyOnUpdate="0" field="lu2_h" expression=""/>
    <default applyOnUpdate="0" field="lu2_type" expression=""/>
    <default applyOnUpdate="0" field="lu2_perc" expression=""/>
    <default applyOnUpdate="0" field="lu2_perc_cls" expression=""/>
    <default applyOnUpdate="0" field="parcel_area_ha" expression=""/>
    <default applyOnUpdate="0" field="tree_height_survey" expression=""/>
    <default applyOnUpdate="0" field="tree_height_maturity" expression=""/>
    <default applyOnUpdate="0" field="feature_width" expression=""/>
    <default applyOnUpdate="0" field="lm_plough_slope" expression=""/>
    <default applyOnUpdate="0" field="lm_plough_direct" expression=""/>
    <default applyOnUpdate="0" field="lm_stone_walls" expression=""/>
    <default applyOnUpdate="0" field="lm_grass_margins" expression=""/>
    <default applyOnUpdate="0" field="cprn_cando" expression=""/>
    <default applyOnUpdate="0" field="cprn_lc" expression=""/>
    <default applyOnUpdate="0" field="cprn_lc1n" expression=""/>
    <default applyOnUpdate="0" field="cprnc_lc1e" expression=""/>
    <default applyOnUpdate="0" field="cprnc_lc1s" expression=""/>
    <default applyOnUpdate="0" field="cprnc_lc1w" expression=""/>
    <default applyOnUpdate="0" field="cprn_lc1n_brdth" expression=""/>
    <default applyOnUpdate="0" field="cprn_lc1e_brdth" expression=""/>
    <default applyOnUpdate="0" field="cprn_lc1s_brdth" expression=""/>
    <default applyOnUpdate="0" field="cprn_lc1w_brdth" expression=""/>
    <default applyOnUpdate="0" field="cprn_lc1n_next" expression=""/>
    <default applyOnUpdate="0" field="cprn_lc1e_next" expression=""/>
    <default applyOnUpdate="0" field="cprn_lc1s_next" expression=""/>
    <default applyOnUpdate="0" field="cprn_lc1w_next" expression=""/>
    <default applyOnUpdate="0" field="cprn_urban" expression=""/>
    <default applyOnUpdate="0" field="cprn_impervious_perc" expression=""/>
    <default applyOnUpdate="0" field="inspire_plcc1" expression=""/>
    <default applyOnUpdate="0" field="inspire_plcc2" expression=""/>
    <default applyOnUpdate="0" field="inspire_plcc3" expression=""/>
    <default applyOnUpdate="0" field="inspire_plcc4" expression=""/>
    <default applyOnUpdate="0" field="inspire_plcc5" expression=""/>
    <default applyOnUpdate="0" field="inspire_plcc6" expression=""/>
    <default applyOnUpdate="0" field="inspire_plcc7" expression=""/>
    <default applyOnUpdate="0" field="inspire_plcc8" expression=""/>
    <default applyOnUpdate="0" field="eunis_complex" expression=""/>
    <default applyOnUpdate="0" field="grassland_sample" expression=""/>
    <default applyOnUpdate="0" field="grass_cando" expression=""/>
    <default applyOnUpdate="0" field="grazing" expression=""/>
    <default applyOnUpdate="0" field="wm" expression=""/>
    <default applyOnUpdate="0" field="wm_source" expression=""/>
    <default applyOnUpdate="0" field="wm_type" expression=""/>
    <default applyOnUpdate="0" field="wm_delivery" expression=""/>
    <default applyOnUpdate="0" field="soil_taken" expression=""/>
    <default applyOnUpdate="0" field="erosion_cando" expression=""/>
    <default applyOnUpdate="0" field="bio_sample" expression=""/>
    <default applyOnUpdate="0" field="soil_bio_taken" expression=""/>
    <default applyOnUpdate="0" field="bulk0_10_sample" expression=""/>
    <default applyOnUpdate="0" field="soil_blk_0_10_taken" expression=""/>
    <default applyOnUpdate="0" field="bulk10_20_sample" expression=""/>
    <default applyOnUpdate="0" field="soil_blk_10_20_taken" expression=""/>
    <default applyOnUpdate="0" field="bulk20_30_sample" expression=""/>
    <default applyOnUpdate="0" field="soil_blk_20_30_taken" expression=""/>
    <default applyOnUpdate="0" field="standard_sample" expression=""/>
    <default applyOnUpdate="0" field="soil_std_taken" expression=""/>
    <default applyOnUpdate="0" field="organic_sample" expression=""/>
    <default applyOnUpdate="0" field="soil_org_depth_cando" expression=""/>
    <default applyOnUpdate="0" field="office_pi" expression=""/>
    <default applyOnUpdate="0" field="pi_extension" expression=""/>
    <default applyOnUpdate="0" field="lndmng_plough" expression=""/>
    <default applyOnUpdate="0" field="special_status" expression=""/>
    <default applyOnUpdate="0" field="lc_lu_special_remark" expression=""/>
    <default applyOnUpdate="0" field="soil_stones_perc" expression=""/>
    <default applyOnUpdate="0" field="soil_stones_perc_cls" expression=""/>
    <default applyOnUpdate="0" field="photo_point" expression=""/>
    <default applyOnUpdate="0" field="photo_north" expression=""/>
    <default applyOnUpdate="0" field="photo_east" expression=""/>
    <default applyOnUpdate="0" field="photo_south" expression=""/>
    <default applyOnUpdate="0" field="photo_west" expression=""/>
    <default applyOnUpdate="0" field="crop_residues" expression=""/>
    <default applyOnUpdate="0" field="transect" expression=""/>
    <default applyOnUpdate="0" field="ex_ante" expression=""/>
    <default applyOnUpdate="0" field="survey_year" expression=""/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" unique_strength="1" constraints="3" notnull_strength="1" field="fid"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="point_id"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="nuts0"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="nuts1"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="nuts2"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="nuts3"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="survey_date"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="car_latitude"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="car_longitude"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="car_ew"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="gps_proj"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="gps_prec"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="gps_lat"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="gps_long"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="gps_altitude"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="th_lat"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="th_long"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="dist_thr_grid"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="obs_dist"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="obs_direct"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="obs_type"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="obs_radius"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lc1"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lc1_h"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lc1_h_l3_missing"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lc1_h_l3_missing_level"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lc1_spec"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lc1_perc"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lc1_perc_cls"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lc2"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lc2_h"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lc2_h_l3_missing"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lc2_h_l3_missing_level"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lc2_spec"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lc2_perc"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lc2_perc_cls"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lu1"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lu1_h"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lu1_type"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lu1_perc"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lu1_perc_cls"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lu2"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lu2_h"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lu2_type"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lu2_perc"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lu2_perc_cls"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="parcel_area_ha"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="tree_height_survey"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="tree_height_maturity"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="feature_width"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lm_plough_slope"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lm_plough_direct"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lm_stone_walls"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lm_grass_margins"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="cprn_cando"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="cprn_lc"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="cprn_lc1n"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="cprnc_lc1e"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="cprnc_lc1s"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="cprnc_lc1w"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="cprn_lc1n_brdth"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="cprn_lc1e_brdth"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="cprn_lc1s_brdth"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="cprn_lc1w_brdth"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="cprn_lc1n_next"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="cprn_lc1e_next"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="cprn_lc1s_next"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="cprn_lc1w_next"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="cprn_urban"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="cprn_impervious_perc"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="inspire_plcc1"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="inspire_plcc2"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="inspire_plcc3"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="inspire_plcc4"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="inspire_plcc5"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="inspire_plcc6"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="inspire_plcc7"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="inspire_plcc8"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="eunis_complex"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="grassland_sample"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="grass_cando"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="grazing"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="wm"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="wm_source"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="wm_type"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="wm_delivery"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="soil_taken"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="erosion_cando"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="bio_sample"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="soil_bio_taken"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="bulk0_10_sample"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="soil_blk_0_10_taken"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="bulk10_20_sample"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="soil_blk_10_20_taken"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="bulk20_30_sample"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="soil_blk_20_30_taken"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="standard_sample"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="soil_std_taken"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="organic_sample"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="soil_org_depth_cando"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="office_pi"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="pi_extension"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lndmng_plough"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="special_status"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="lc_lu_special_remark"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="soil_stones_perc"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="soil_stones_perc_cls"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="photo_point"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="photo_north"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="photo_east"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="photo_south"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="photo_west"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="crop_residues"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="transect"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="ex_ante"/>
    <constraint exp_strength="0" unique_strength="0" constraints="0" notnull_strength="0" field="survey_year"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="fid"/>
    <constraint desc="" exp="" field="point_id"/>
    <constraint desc="" exp="" field="nuts0"/>
    <constraint desc="" exp="" field="nuts1"/>
    <constraint desc="" exp="" field="nuts2"/>
    <constraint desc="" exp="" field="nuts3"/>
    <constraint desc="" exp="" field="survey_date"/>
    <constraint desc="" exp="" field="car_latitude"/>
    <constraint desc="" exp="" field="car_longitude"/>
    <constraint desc="" exp="" field="car_ew"/>
    <constraint desc="" exp="" field="gps_proj"/>
    <constraint desc="" exp="" field="gps_prec"/>
    <constraint desc="" exp="" field="gps_lat"/>
    <constraint desc="" exp="" field="gps_long"/>
    <constraint desc="" exp="" field="gps_altitude"/>
    <constraint desc="" exp="" field="th_lat"/>
    <constraint desc="" exp="" field="th_long"/>
    <constraint desc="" exp="" field="dist_thr_grid"/>
    <constraint desc="" exp="" field="obs_dist"/>
    <constraint desc="" exp="" field="obs_direct"/>
    <constraint desc="" exp="" field="obs_type"/>
    <constraint desc="" exp="" field="obs_radius"/>
    <constraint desc="" exp="" field="lc1"/>
    <constraint desc="" exp="" field="lc1_h"/>
    <constraint desc="" exp="" field="lc1_h_l3_missing"/>
    <constraint desc="" exp="" field="lc1_h_l3_missing_level"/>
    <constraint desc="" exp="" field="lc1_spec"/>
    <constraint desc="" exp="" field="lc1_perc"/>
    <constraint desc="" exp="" field="lc1_perc_cls"/>
    <constraint desc="" exp="" field="lc2"/>
    <constraint desc="" exp="" field="lc2_h"/>
    <constraint desc="" exp="" field="lc2_h_l3_missing"/>
    <constraint desc="" exp="" field="lc2_h_l3_missing_level"/>
    <constraint desc="" exp="" field="lc2_spec"/>
    <constraint desc="" exp="" field="lc2_perc"/>
    <constraint desc="" exp="" field="lc2_perc_cls"/>
    <constraint desc="" exp="" field="lu1"/>
    <constraint desc="" exp="" field="lu1_h"/>
    <constraint desc="" exp="" field="lu1_type"/>
    <constraint desc="" exp="" field="lu1_perc"/>
    <constraint desc="" exp="" field="lu1_perc_cls"/>
    <constraint desc="" exp="" field="lu2"/>
    <constraint desc="" exp="" field="lu2_h"/>
    <constraint desc="" exp="" field="lu2_type"/>
    <constraint desc="" exp="" field="lu2_perc"/>
    <constraint desc="" exp="" field="lu2_perc_cls"/>
    <constraint desc="" exp="" field="parcel_area_ha"/>
    <constraint desc="" exp="" field="tree_height_survey"/>
    <constraint desc="" exp="" field="tree_height_maturity"/>
    <constraint desc="" exp="" field="feature_width"/>
    <constraint desc="" exp="" field="lm_plough_slope"/>
    <constraint desc="" exp="" field="lm_plough_direct"/>
    <constraint desc="" exp="" field="lm_stone_walls"/>
    <constraint desc="" exp="" field="lm_grass_margins"/>
    <constraint desc="" exp="" field="cprn_cando"/>
    <constraint desc="" exp="" field="cprn_lc"/>
    <constraint desc="" exp="" field="cprn_lc1n"/>
    <constraint desc="" exp="" field="cprnc_lc1e"/>
    <constraint desc="" exp="" field="cprnc_lc1s"/>
    <constraint desc="" exp="" field="cprnc_lc1w"/>
    <constraint desc="" exp="" field="cprn_lc1n_brdth"/>
    <constraint desc="" exp="" field="cprn_lc1e_brdth"/>
    <constraint desc="" exp="" field="cprn_lc1s_brdth"/>
    <constraint desc="" exp="" field="cprn_lc1w_brdth"/>
    <constraint desc="" exp="" field="cprn_lc1n_next"/>
    <constraint desc="" exp="" field="cprn_lc1e_next"/>
    <constraint desc="" exp="" field="cprn_lc1s_next"/>
    <constraint desc="" exp="" field="cprn_lc1w_next"/>
    <constraint desc="" exp="" field="cprn_urban"/>
    <constraint desc="" exp="" field="cprn_impervious_perc"/>
    <constraint desc="" exp="" field="inspire_plcc1"/>
    <constraint desc="" exp="" field="inspire_plcc2"/>
    <constraint desc="" exp="" field="inspire_plcc3"/>
    <constraint desc="" exp="" field="inspire_plcc4"/>
    <constraint desc="" exp="" field="inspire_plcc5"/>
    <constraint desc="" exp="" field="inspire_plcc6"/>
    <constraint desc="" exp="" field="inspire_plcc7"/>
    <constraint desc="" exp="" field="inspire_plcc8"/>
    <constraint desc="" exp="" field="eunis_complex"/>
    <constraint desc="" exp="" field="grassland_sample"/>
    <constraint desc="" exp="" field="grass_cando"/>
    <constraint desc="" exp="" field="grazing"/>
    <constraint desc="" exp="" field="wm"/>
    <constraint desc="" exp="" field="wm_source"/>
    <constraint desc="" exp="" field="wm_type"/>
    <constraint desc="" exp="" field="wm_delivery"/>
    <constraint desc="" exp="" field="soil_taken"/>
    <constraint desc="" exp="" field="erosion_cando"/>
    <constraint desc="" exp="" field="bio_sample"/>
    <constraint desc="" exp="" field="soil_bio_taken"/>
    <constraint desc="" exp="" field="bulk0_10_sample"/>
    <constraint desc="" exp="" field="soil_blk_0_10_taken"/>
    <constraint desc="" exp="" field="bulk10_20_sample"/>
    <constraint desc="" exp="" field="soil_blk_10_20_taken"/>
    <constraint desc="" exp="" field="bulk20_30_sample"/>
    <constraint desc="" exp="" field="soil_blk_20_30_taken"/>
    <constraint desc="" exp="" field="standard_sample"/>
    <constraint desc="" exp="" field="soil_std_taken"/>
    <constraint desc="" exp="" field="organic_sample"/>
    <constraint desc="" exp="" field="soil_org_depth_cando"/>
    <constraint desc="" exp="" field="office_pi"/>
    <constraint desc="" exp="" field="pi_extension"/>
    <constraint desc="" exp="" field="lndmng_plough"/>
    <constraint desc="" exp="" field="special_status"/>
    <constraint desc="" exp="" field="lc_lu_special_remark"/>
    <constraint desc="" exp="" field="soil_stones_perc"/>
    <constraint desc="" exp="" field="soil_stones_perc_cls"/>
    <constraint desc="" exp="" field="photo_point"/>
    <constraint desc="" exp="" field="photo_north"/>
    <constraint desc="" exp="" field="photo_east"/>
    <constraint desc="" exp="" field="photo_south"/>
    <constraint desc="" exp="" field="photo_west"/>
    <constraint desc="" exp="" field="crop_residues"/>
    <constraint desc="" exp="" field="transect"/>
    <constraint desc="" exp="" field="ex_ante"/>
    <constraint desc="" exp="" field="survey_year"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column hidden="0" type="field" name="fid" width="-1"/>
      <column hidden="0" type="field" name="point_id" width="-1"/>
      <column hidden="0" type="field" name="nuts0" width="-1"/>
      <column hidden="0" type="field" name="nuts1" width="-1"/>
      <column hidden="0" type="field" name="nuts2" width="-1"/>
      <column hidden="0" type="field" name="nuts3" width="-1"/>
      <column hidden="0" type="field" name="survey_date" width="-1"/>
      <column hidden="0" type="field" name="car_latitude" width="-1"/>
      <column hidden="0" type="field" name="car_longitude" width="-1"/>
      <column hidden="0" type="field" name="car_ew" width="-1"/>
      <column hidden="0" type="field" name="gps_proj" width="-1"/>
      <column hidden="0" type="field" name="gps_prec" width="-1"/>
      <column hidden="0" type="field" name="gps_lat" width="-1"/>
      <column hidden="0" type="field" name="gps_long" width="-1"/>
      <column hidden="0" type="field" name="gps_altitude" width="-1"/>
      <column hidden="0" type="field" name="th_lat" width="-1"/>
      <column hidden="0" type="field" name="th_long" width="-1"/>
      <column hidden="0" type="field" name="dist_thr_grid" width="-1"/>
      <column hidden="0" type="field" name="obs_dist" width="-1"/>
      <column hidden="0" type="field" name="obs_direct" width="-1"/>
      <column hidden="0" type="field" name="obs_type" width="-1"/>
      <column hidden="0" type="field" name="obs_radius" width="-1"/>
      <column hidden="0" type="field" name="lc1" width="-1"/>
      <column hidden="0" type="field" name="lc1_h" width="-1"/>
      <column hidden="0" type="field" name="lc1_h_l3_missing" width="-1"/>
      <column hidden="0" type="field" name="lc1_h_l3_missing_level" width="-1"/>
      <column hidden="0" type="field" name="lc1_spec" width="-1"/>
      <column hidden="0" type="field" name="lc1_perc" width="-1"/>
      <column hidden="0" type="field" name="lc2" width="-1"/>
      <column hidden="0" type="field" name="lc2_h" width="-1"/>
      <column hidden="0" type="field" name="lc2_h_l3_missing" width="-1"/>
      <column hidden="0" type="field" name="lc2_h_l3_missing_level" width="-1"/>
      <column hidden="0" type="field" name="lc2_spec" width="-1"/>
      <column hidden="0" type="field" name="lc2_perc" width="-1"/>
      <column hidden="0" type="field" name="lu1" width="-1"/>
      <column hidden="0" type="field" name="lu1_h" width="-1"/>
      <column hidden="0" type="field" name="lu1_type" width="-1"/>
      <column hidden="0" type="field" name="lu1_perc" width="-1"/>
      <column hidden="0" type="field" name="lu2" width="-1"/>
      <column hidden="0" type="field" name="lu2_h" width="-1"/>
      <column hidden="0" type="field" name="lu2_type" width="-1"/>
      <column hidden="0" type="field" name="lu2_perc" width="-1"/>
      <column hidden="0" type="field" name="parcel_area_ha" width="-1"/>
      <column hidden="0" type="field" name="tree_height_survey" width="-1"/>
      <column hidden="0" type="field" name="tree_height_maturity" width="-1"/>
      <column hidden="0" type="field" name="feature_width" width="-1"/>
      <column hidden="0" type="field" name="lm_plough_slope" width="-1"/>
      <column hidden="0" type="field" name="lm_plough_direct" width="-1"/>
      <column hidden="0" type="field" name="lm_stone_walls" width="-1"/>
      <column hidden="0" type="field" name="lm_grass_margins" width="-1"/>
      <column hidden="0" type="field" name="cprn_cando" width="-1"/>
      <column hidden="0" type="field" name="cprn_lc" width="-1"/>
      <column hidden="0" type="field" name="cprn_lc1n" width="-1"/>
      <column hidden="0" type="field" name="cprnc_lc1e" width="-1"/>
      <column hidden="0" type="field" name="cprnc_lc1s" width="-1"/>
      <column hidden="0" type="field" name="cprnc_lc1w" width="-1"/>
      <column hidden="0" type="field" name="cprn_lc1n_brdth" width="-1"/>
      <column hidden="0" type="field" name="cprn_lc1e_brdth" width="-1"/>
      <column hidden="0" type="field" name="cprn_lc1s_brdth" width="-1"/>
      <column hidden="0" type="field" name="cprn_lc1w_brdth" width="-1"/>
      <column hidden="0" type="field" name="cprn_lc1n_next" width="-1"/>
      <column hidden="0" type="field" name="cprn_lc1e_next" width="-1"/>
      <column hidden="0" type="field" name="cprn_lc1s_next" width="-1"/>
      <column hidden="0" type="field" name="cprn_lc1w_next" width="-1"/>
      <column hidden="0" type="field" name="cprn_urban" width="-1"/>
      <column hidden="0" type="field" name="cprn_impervious_perc" width="-1"/>
      <column hidden="0" type="field" name="inspire_plcc1" width="-1"/>
      <column hidden="0" type="field" name="inspire_plcc2" width="-1"/>
      <column hidden="0" type="field" name="inspire_plcc3" width="-1"/>
      <column hidden="0" type="field" name="inspire_plcc4" width="-1"/>
      <column hidden="0" type="field" name="inspire_plcc5" width="-1"/>
      <column hidden="0" type="field" name="inspire_plcc6" width="-1"/>
      <column hidden="0" type="field" name="inspire_plcc7" width="-1"/>
      <column hidden="0" type="field" name="inspire_plcc8" width="-1"/>
      <column hidden="0" type="field" name="eunis_complex" width="-1"/>
      <column hidden="0" type="field" name="grassland_sample" width="-1"/>
      <column hidden="0" type="field" name="grass_cando" width="-1"/>
      <column hidden="0" type="field" name="grazing" width="-1"/>
      <column hidden="0" type="field" name="wm" width="-1"/>
      <column hidden="0" type="field" name="wm_source" width="-1"/>
      <column hidden="0" type="field" name="wm_type" width="-1"/>
      <column hidden="0" type="field" name="wm_delivery" width="-1"/>
      <column hidden="0" type="field" name="soil_taken" width="-1"/>
      <column hidden="0" type="field" name="erosion_cando" width="-1"/>
      <column hidden="0" type="field" name="bio_sample" width="-1"/>
      <column hidden="0" type="field" name="soil_bio_taken" width="-1"/>
      <column hidden="0" type="field" name="bulk0_10_sample" width="-1"/>
      <column hidden="0" type="field" name="soil_blk_0_10_taken" width="-1"/>
      <column hidden="0" type="field" name="bulk10_20_sample" width="-1"/>
      <column hidden="0" type="field" name="soil_blk_10_20_taken" width="-1"/>
      <column hidden="0" type="field" name="bulk20_30_sample" width="-1"/>
      <column hidden="0" type="field" name="soil_blk_20_30_taken" width="-1"/>
      <column hidden="0" type="field" name="standard_sample" width="-1"/>
      <column hidden="0" type="field" name="soil_std_taken" width="-1"/>
      <column hidden="0" type="field" name="organic_sample" width="-1"/>
      <column hidden="0" type="field" name="soil_org_depth_cando" width="-1"/>
      <column hidden="0" type="field" name="office_pi" width="-1"/>
      <column hidden="0" type="field" name="lndmng_plough" width="-1"/>
      <column hidden="0" type="field" name="special_status" width="-1"/>
      <column hidden="0" type="field" name="lc_lu_special_remark" width="-1"/>
      <column hidden="0" type="field" name="soil_stones_perc" width="-1"/>
      <column hidden="0" type="field" name="photo_point" width="-1"/>
      <column hidden="0" type="field" name="photo_north" width="-1"/>
      <column hidden="0" type="field" name="photo_east" width="-1"/>
      <column hidden="0" type="field" name="photo_south" width="-1"/>
      <column hidden="0" type="field" name="photo_west" width="-1"/>
      <column hidden="0" type="field" name="crop_residues" width="-1"/>
      <column hidden="0" type="field" name="transect" width="-1"/>
      <column hidden="0" type="field" name="ex_ante" width="-1"/>
      <column hidden="0" type="field" name="survey_year" width="-1"/>
      <column hidden="0" type="field" name="lc1_perc_cls" width="-1"/>
      <column hidden="0" type="field" name="lc2_perc_cls" width="-1"/>
      <column hidden="0" type="field" name="lu1_perc_cls" width="-1"/>
      <column hidden="0" type="field" name="lu2_perc_cls" width="-1"/>
      <column hidden="0" type="field" name="pi_extension" width="-1"/>
      <column hidden="0" type="field" name="soil_stones_perc_cls" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="bio_sample" editable="1"/>
    <field name="bulk0_10_sample" editable="1"/>
    <field name="bulk10_20_sample" editable="1"/>
    <field name="bulk20_30_sample" editable="1"/>
    <field name="car_ew" editable="1"/>
    <field name="car_latitude" editable="1"/>
    <field name="car_longitude" editable="1"/>
    <field name="cprn_cando" editable="1"/>
    <field name="cprn_impervious_perc" editable="1"/>
    <field name="cprn_lc" editable="1"/>
    <field name="cprn_lc1e_brdth" editable="1"/>
    <field name="cprn_lc1e_next" editable="1"/>
    <field name="cprn_lc1n" editable="1"/>
    <field name="cprn_lc1n_brdth" editable="1"/>
    <field name="cprn_lc1n_next" editable="1"/>
    <field name="cprn_lc1s_brdth" editable="1"/>
    <field name="cprn_lc1s_next" editable="1"/>
    <field name="cprn_lc1w_brdth" editable="1"/>
    <field name="cprn_lc1w_next" editable="1"/>
    <field name="cprn_urban" editable="1"/>
    <field name="cprnc_lc1e" editable="1"/>
    <field name="cprnc_lc1s" editable="1"/>
    <field name="cprnc_lc1w" editable="1"/>
    <field name="crop_residues" editable="1"/>
    <field name="dist_gps_th" editable="1"/>
    <field name="dist_th_thr" editable="1"/>
    <field name="dist_thr_grid" editable="1"/>
    <field name="erosion_cando" editable="1"/>
    <field name="eunis_complex" editable="1"/>
    <field name="ex_ante" editable="1"/>
    <field name="feature_width" editable="1"/>
    <field name="fid" editable="1"/>
    <field name="gps_altitude" editable="1"/>
    <field name="gps_lat" editable="1"/>
    <field name="gps_long" editable="1"/>
    <field name="gps_prec" editable="1"/>
    <field name="gps_proj" editable="1"/>
    <field name="grass_cando" editable="1"/>
    <field name="grassland_sample" editable="1"/>
    <field name="grazing" editable="1"/>
    <field name="inspire_plcc1" editable="1"/>
    <field name="inspire_plcc2" editable="1"/>
    <field name="inspire_plcc3" editable="1"/>
    <field name="inspire_plcc4" editable="1"/>
    <field name="inspire_plcc5" editable="1"/>
    <field name="inspire_plcc6" editable="1"/>
    <field name="inspire_plcc7" editable="1"/>
    <field name="inspire_plcc8" editable="1"/>
    <field name="lc1" editable="1"/>
    <field name="lc1_h" editable="1"/>
    <field name="lc1_h_l3_missing" editable="1"/>
    <field name="lc1_h_l3_missing_level" editable="1"/>
    <field name="lc1_perc" editable="1"/>
    <field name="lc1_perc_cls" editable="1"/>
    <field name="lc1_spec" editable="1"/>
    <field name="lc2" editable="1"/>
    <field name="lc2_h" editable="1"/>
    <field name="lc2_h_l3_missing" editable="1"/>
    <field name="lc2_h_l3_missing_level" editable="1"/>
    <field name="lc2_perc" editable="1"/>
    <field name="lc2_perc_cls" editable="1"/>
    <field name="lc2_spec" editable="1"/>
    <field name="lc_lu_special_remark" editable="1"/>
    <field name="lm_grass_margins" editable="1"/>
    <field name="lm_plough_direct" editable="1"/>
    <field name="lm_plough_slope" editable="1"/>
    <field name="lm_stone_walls" editable="1"/>
    <field name="lndmng_plough" editable="1"/>
    <field name="lu1" editable="1"/>
    <field name="lu1_h" editable="1"/>
    <field name="lu1_perc" editable="1"/>
    <field name="lu1_perc_cls" editable="1"/>
    <field name="lu1_type" editable="1"/>
    <field name="lu2" editable="1"/>
    <field name="lu2_h" editable="1"/>
    <field name="lu2_perc" editable="1"/>
    <field name="lu2_perc_cls" editable="1"/>
    <field name="lu2_type" editable="1"/>
    <field name="nuts0" editable="1"/>
    <field name="nuts1" editable="1"/>
    <field name="nuts2" editable="1"/>
    <field name="nuts3" editable="1"/>
    <field name="obs_direct" editable="1"/>
    <field name="obs_dist" editable="1"/>
    <field name="obs_radius" editable="1"/>
    <field name="obs_type" editable="1"/>
    <field name="office_pi" editable="1"/>
    <field name="organic_sample" editable="1"/>
    <field name="parcel_area_ha" editable="1"/>
    <field name="photo_east" editable="1"/>
    <field name="photo_north" editable="1"/>
    <field name="photo_point" editable="1"/>
    <field name="photo_south" editable="1"/>
    <field name="photo_west" editable="1"/>
    <field name="pi_extension" editable="1"/>
    <field name="point_id" editable="1"/>
    <field name="soil_bio_taken" editable="1"/>
    <field name="soil_blk_0_10_taken" editable="1"/>
    <field name="soil_blk_10_20_taken" editable="1"/>
    <field name="soil_blk_20_30_taken" editable="1"/>
    <field name="soil_org_depth_cando" editable="1"/>
    <field name="soil_std_taken" editable="1"/>
    <field name="soil_stones_perc" editable="1"/>
    <field name="soil_stones_perc_cls" editable="1"/>
    <field name="soil_taken" editable="1"/>
    <field name="special_status" editable="1"/>
    <field name="standard_sample" editable="1"/>
    <field name="survey_date" editable="1"/>
    <field name="survey_year" editable="1"/>
    <field name="th_lat" editable="1"/>
    <field name="th_long" editable="1"/>
    <field name="transect" editable="1"/>
    <field name="tree_height_maturity" editable="1"/>
    <field name="tree_height_survey" editable="1"/>
    <field name="wm" editable="1"/>
    <field name="wm_delivery" editable="1"/>
    <field name="wm_source" editable="1"/>
    <field name="wm_type" editable="1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="bio_sample"/>
    <field labelOnTop="0" name="bulk0_10_sample"/>
    <field labelOnTop="0" name="bulk10_20_sample"/>
    <field labelOnTop="0" name="bulk20_30_sample"/>
    <field labelOnTop="0" name="car_ew"/>
    <field labelOnTop="0" name="car_latitude"/>
    <field labelOnTop="0" name="car_longitude"/>
    <field labelOnTop="0" name="cprn_cando"/>
    <field labelOnTop="0" name="cprn_impervious_perc"/>
    <field labelOnTop="0" name="cprn_lc"/>
    <field labelOnTop="0" name="cprn_lc1e_brdth"/>
    <field labelOnTop="0" name="cprn_lc1e_next"/>
    <field labelOnTop="0" name="cprn_lc1n"/>
    <field labelOnTop="0" name="cprn_lc1n_brdth"/>
    <field labelOnTop="0" name="cprn_lc1n_next"/>
    <field labelOnTop="0" name="cprn_lc1s_brdth"/>
    <field labelOnTop="0" name="cprn_lc1s_next"/>
    <field labelOnTop="0" name="cprn_lc1w_brdth"/>
    <field labelOnTop="0" name="cprn_lc1w_next"/>
    <field labelOnTop="0" name="cprn_urban"/>
    <field labelOnTop="0" name="cprnc_lc1e"/>
    <field labelOnTop="0" name="cprnc_lc1s"/>
    <field labelOnTop="0" name="cprnc_lc1w"/>
    <field labelOnTop="0" name="crop_residues"/>
    <field labelOnTop="0" name="dist_gps_th"/>
    <field labelOnTop="0" name="dist_th_thr"/>
    <field labelOnTop="0" name="dist_thr_grid"/>
    <field labelOnTop="0" name="erosion_cando"/>
    <field labelOnTop="0" name="eunis_complex"/>
    <field labelOnTop="0" name="ex_ante"/>
    <field labelOnTop="0" name="feature_width"/>
    <field labelOnTop="0" name="fid"/>
    <field labelOnTop="0" name="gps_altitude"/>
    <field labelOnTop="0" name="gps_lat"/>
    <field labelOnTop="0" name="gps_long"/>
    <field labelOnTop="0" name="gps_prec"/>
    <field labelOnTop="0" name="gps_proj"/>
    <field labelOnTop="0" name="grass_cando"/>
    <field labelOnTop="0" name="grassland_sample"/>
    <field labelOnTop="0" name="grazing"/>
    <field labelOnTop="0" name="inspire_plcc1"/>
    <field labelOnTop="0" name="inspire_plcc2"/>
    <field labelOnTop="0" name="inspire_plcc3"/>
    <field labelOnTop="0" name="inspire_plcc4"/>
    <field labelOnTop="0" name="inspire_plcc5"/>
    <field labelOnTop="0" name="inspire_plcc6"/>
    <field labelOnTop="0" name="inspire_plcc7"/>
    <field labelOnTop="0" name="inspire_plcc8"/>
    <field labelOnTop="0" name="lc1"/>
    <field labelOnTop="0" name="lc1_h"/>
    <field labelOnTop="0" name="lc1_h_l3_missing"/>
    <field labelOnTop="0" name="lc1_h_l3_missing_level"/>
    <field labelOnTop="0" name="lc1_perc"/>
    <field labelOnTop="0" name="lc1_perc_cls"/>
    <field labelOnTop="0" name="lc1_spec"/>
    <field labelOnTop="0" name="lc2"/>
    <field labelOnTop="0" name="lc2_h"/>
    <field labelOnTop="0" name="lc2_h_l3_missing"/>
    <field labelOnTop="0" name="lc2_h_l3_missing_level"/>
    <field labelOnTop="0" name="lc2_perc"/>
    <field labelOnTop="0" name="lc2_perc_cls"/>
    <field labelOnTop="0" name="lc2_spec"/>
    <field labelOnTop="0" name="lc_lu_special_remark"/>
    <field labelOnTop="0" name="lm_grass_margins"/>
    <field labelOnTop="0" name="lm_plough_direct"/>
    <field labelOnTop="0" name="lm_plough_slope"/>
    <field labelOnTop="0" name="lm_stone_walls"/>
    <field labelOnTop="0" name="lndmng_plough"/>
    <field labelOnTop="0" name="lu1"/>
    <field labelOnTop="0" name="lu1_h"/>
    <field labelOnTop="0" name="lu1_perc"/>
    <field labelOnTop="0" name="lu1_perc_cls"/>
    <field labelOnTop="0" name="lu1_type"/>
    <field labelOnTop="0" name="lu2"/>
    <field labelOnTop="0" name="lu2_h"/>
    <field labelOnTop="0" name="lu2_perc"/>
    <field labelOnTop="0" name="lu2_perc_cls"/>
    <field labelOnTop="0" name="lu2_type"/>
    <field labelOnTop="0" name="nuts0"/>
    <field labelOnTop="0" name="nuts1"/>
    <field labelOnTop="0" name="nuts2"/>
    <field labelOnTop="0" name="nuts3"/>
    <field labelOnTop="0" name="obs_direct"/>
    <field labelOnTop="0" name="obs_dist"/>
    <field labelOnTop="0" name="obs_radius"/>
    <field labelOnTop="0" name="obs_type"/>
    <field labelOnTop="0" name="office_pi"/>
    <field labelOnTop="0" name="organic_sample"/>
    <field labelOnTop="0" name="parcel_area_ha"/>
    <field labelOnTop="0" name="photo_east"/>
    <field labelOnTop="0" name="photo_north"/>
    <field labelOnTop="0" name="photo_point"/>
    <field labelOnTop="0" name="photo_south"/>
    <field labelOnTop="0" name="photo_west"/>
    <field labelOnTop="0" name="pi_extension"/>
    <field labelOnTop="0" name="point_id"/>
    <field labelOnTop="0" name="soil_bio_taken"/>
    <field labelOnTop="0" name="soil_blk_0_10_taken"/>
    <field labelOnTop="0" name="soil_blk_10_20_taken"/>
    <field labelOnTop="0" name="soil_blk_20_30_taken"/>
    <field labelOnTop="0" name="soil_org_depth_cando"/>
    <field labelOnTop="0" name="soil_std_taken"/>
    <field labelOnTop="0" name="soil_stones_perc"/>
    <field labelOnTop="0" name="soil_stones_perc_cls"/>
    <field labelOnTop="0" name="soil_taken"/>
    <field labelOnTop="0" name="special_status"/>
    <field labelOnTop="0" name="standard_sample"/>
    <field labelOnTop="0" name="survey_date"/>
    <field labelOnTop="0" name="survey_year"/>
    <field labelOnTop="0" name="th_lat"/>
    <field labelOnTop="0" name="th_long"/>
    <field labelOnTop="0" name="transect"/>
    <field labelOnTop="0" name="tree_height_maturity"/>
    <field labelOnTop="0" name="tree_height_survey"/>
    <field labelOnTop="0" name="wm"/>
    <field labelOnTop="0" name="wm_delivery"/>
    <field labelOnTop="0" name="wm_source"/>
    <field labelOnTop="0" name="wm_type"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="bio_sample"/>
    <field reuseLastValue="0" name="bulk0_10_sample"/>
    <field reuseLastValue="0" name="bulk10_20_sample"/>
    <field reuseLastValue="0" name="bulk20_30_sample"/>
    <field reuseLastValue="0" name="car_ew"/>
    <field reuseLastValue="0" name="car_latitude"/>
    <field reuseLastValue="0" name="car_longitude"/>
    <field reuseLastValue="0" name="cprn_cando"/>
    <field reuseLastValue="0" name="cprn_impervious_perc"/>
    <field reuseLastValue="0" name="cprn_lc"/>
    <field reuseLastValue="0" name="cprn_lc1e_brdth"/>
    <field reuseLastValue="0" name="cprn_lc1e_next"/>
    <field reuseLastValue="0" name="cprn_lc1n"/>
    <field reuseLastValue="0" name="cprn_lc1n_brdth"/>
    <field reuseLastValue="0" name="cprn_lc1n_next"/>
    <field reuseLastValue="0" name="cprn_lc1s_brdth"/>
    <field reuseLastValue="0" name="cprn_lc1s_next"/>
    <field reuseLastValue="0" name="cprn_lc1w_brdth"/>
    <field reuseLastValue="0" name="cprn_lc1w_next"/>
    <field reuseLastValue="0" name="cprn_urban"/>
    <field reuseLastValue="0" name="cprnc_lc1e"/>
    <field reuseLastValue="0" name="cprnc_lc1s"/>
    <field reuseLastValue="0" name="cprnc_lc1w"/>
    <field reuseLastValue="0" name="crop_residues"/>
    <field reuseLastValue="0" name="dist_thr_grid"/>
    <field reuseLastValue="0" name="erosion_cando"/>
    <field reuseLastValue="0" name="eunis_complex"/>
    <field reuseLastValue="0" name="ex_ante"/>
    <field reuseLastValue="0" name="feature_width"/>
    <field reuseLastValue="0" name="fid"/>
    <field reuseLastValue="0" name="gps_altitude"/>
    <field reuseLastValue="0" name="gps_lat"/>
    <field reuseLastValue="0" name="gps_long"/>
    <field reuseLastValue="0" name="gps_prec"/>
    <field reuseLastValue="0" name="gps_proj"/>
    <field reuseLastValue="0" name="grass_cando"/>
    <field reuseLastValue="0" name="grassland_sample"/>
    <field reuseLastValue="0" name="grazing"/>
    <field reuseLastValue="0" name="inspire_plcc1"/>
    <field reuseLastValue="0" name="inspire_plcc2"/>
    <field reuseLastValue="0" name="inspire_plcc3"/>
    <field reuseLastValue="0" name="inspire_plcc4"/>
    <field reuseLastValue="0" name="inspire_plcc5"/>
    <field reuseLastValue="0" name="inspire_plcc6"/>
    <field reuseLastValue="0" name="inspire_plcc7"/>
    <field reuseLastValue="0" name="inspire_plcc8"/>
    <field reuseLastValue="0" name="lc1"/>
    <field reuseLastValue="0" name="lc1_h"/>
    <field reuseLastValue="0" name="lc1_h_l3_missing"/>
    <field reuseLastValue="0" name="lc1_h_l3_missing_level"/>
    <field reuseLastValue="0" name="lc1_perc"/>
    <field reuseLastValue="0" name="lc1_perc_cls"/>
    <field reuseLastValue="0" name="lc1_spec"/>
    <field reuseLastValue="0" name="lc2"/>
    <field reuseLastValue="0" name="lc2_h"/>
    <field reuseLastValue="0" name="lc2_h_l3_missing"/>
    <field reuseLastValue="0" name="lc2_h_l3_missing_level"/>
    <field reuseLastValue="0" name="lc2_perc"/>
    <field reuseLastValue="0" name="lc2_perc_cls"/>
    <field reuseLastValue="0" name="lc2_spec"/>
    <field reuseLastValue="0" name="lc_lu_special_remark"/>
    <field reuseLastValue="0" name="lm_grass_margins"/>
    <field reuseLastValue="0" name="lm_plough_direct"/>
    <field reuseLastValue="0" name="lm_plough_slope"/>
    <field reuseLastValue="0" name="lm_stone_walls"/>
    <field reuseLastValue="0" name="lndmng_plough"/>
    <field reuseLastValue="0" name="lu1"/>
    <field reuseLastValue="0" name="lu1_h"/>
    <field reuseLastValue="0" name="lu1_perc"/>
    <field reuseLastValue="0" name="lu1_perc_cls"/>
    <field reuseLastValue="0" name="lu1_type"/>
    <field reuseLastValue="0" name="lu2"/>
    <field reuseLastValue="0" name="lu2_h"/>
    <field reuseLastValue="0" name="lu2_perc"/>
    <field reuseLastValue="0" name="lu2_perc_cls"/>
    <field reuseLastValue="0" name="lu2_type"/>
    <field reuseLastValue="0" name="nuts0"/>
    <field reuseLastValue="0" name="nuts1"/>
    <field reuseLastValue="0" name="nuts2"/>
    <field reuseLastValue="0" name="nuts3"/>
    <field reuseLastValue="0" name="obs_direct"/>
    <field reuseLastValue="0" name="obs_dist"/>
    <field reuseLastValue="0" name="obs_radius"/>
    <field reuseLastValue="0" name="obs_type"/>
    <field reuseLastValue="0" name="office_pi"/>
    <field reuseLastValue="0" name="organic_sample"/>
    <field reuseLastValue="0" name="parcel_area_ha"/>
    <field reuseLastValue="0" name="photo_east"/>
    <field reuseLastValue="0" name="photo_north"/>
    <field reuseLastValue="0" name="photo_point"/>
    <field reuseLastValue="0" name="photo_south"/>
    <field reuseLastValue="0" name="photo_west"/>
    <field reuseLastValue="0" name="pi_extension"/>
    <field reuseLastValue="0" name="point_id"/>
    <field reuseLastValue="0" name="soil_bio_taken"/>
    <field reuseLastValue="0" name="soil_blk_0_10_taken"/>
    <field reuseLastValue="0" name="soil_blk_10_20_taken"/>
    <field reuseLastValue="0" name="soil_blk_20_30_taken"/>
    <field reuseLastValue="0" name="soil_org_depth_cando"/>
    <field reuseLastValue="0" name="soil_std_taken"/>
    <field reuseLastValue="0" name="soil_stones_perc"/>
    <field reuseLastValue="0" name="soil_stones_perc_cls"/>
    <field reuseLastValue="0" name="soil_taken"/>
    <field reuseLastValue="0" name="special_status"/>
    <field reuseLastValue="0" name="standard_sample"/>
    <field reuseLastValue="0" name="survey_date"/>
    <field reuseLastValue="0" name="survey_year"/>
    <field reuseLastValue="0" name="th_lat"/>
    <field reuseLastValue="0" name="th_long"/>
    <field reuseLastValue="0" name="transect"/>
    <field reuseLastValue="0" name="tree_height_maturity"/>
    <field reuseLastValue="0" name="tree_height_survey"/>
    <field reuseLastValue="0" name="wm"/>
    <field reuseLastValue="0" name="wm_delivery"/>
    <field reuseLastValue="0" name="wm_source"/>
    <field reuseLastValue="0" name="wm_type"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"nuts0"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>0</layerGeometryType>
</qgis>
